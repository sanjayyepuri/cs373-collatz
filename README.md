# CS 373: Software Engineering Collatz Repo

* Name: (your Full Name)
  Sai Sanjay Yepuri

* EID: (your EID)
  SSY365

* GitLab ID: (your GitLab ID)
  sanjayyepuri

* HackerRank ID: (your HackerRank ID)
  sanjay_yepuri

* Git SHA: (most recent Git SHA, final change to your repo will change this, that's ok)
  06810cc08a3648e19ee64b9a4c7adb45635d1abc

* GitLab Pipelines: (link to your GitLab CI Pipeline)
  https://gitlab.com/sanjayyepuri/cs373-collatz/-/pipelines
* Estimated completion time: (estimated time in hours, int or float)
  4 hours

* Actual completion time: (actual time in hours, int or float)
  8 hours

* Comments: (any additional comments you have)
  It was not clear to me in the workflow whether we were supposed to make issues based off the steps in the workflow or find issues with the workflow itself. Please ignore the issues marked "Invalid/Ignore." It wasn't clear in the workflow where the git log should be stored; I placed it in a file called git-log.txt

commit 45cb61b18ec854010a90c63f48a4087601291e1e
Author: Sanjay Yepuri <sanjay.yepuri@gmail.com>
Date:   Mon Sep 14 20:49:10 2020 -0500

    updated the readme

commit 06810cc08a3648e19ee64b9a4c7adb45635d1abc
Author: Sanjay Yepuri <sanjay.yepuri@gmail.com>
Date:   Mon Sep 14 20:43:56 2020 -0500

    added a few more inline commments

commit 48437298db1432f413cf07ae30601a186ab691af
Author: Sanjay Yepuri <sanjay.yepuri@gmail.com>
Date:   Mon Sep 14 20:40:10 2020 -0500

    ran pydoc

commit 167e31f915fb548a73867a7268269228f4fe6830
Author: Sanjay Yepuri <sanjay.yepuri@gmail.com>
Date:   Mon Sep 14 11:21:13 2020 -0500

    formatted code and generated test data

commit 94f6bbe092b4ca1154f48d4b7de5575dbd0d0b1a
Author: Sanjay Yepuri <sanjay.yepuri@gmail.com>
Date:   Mon Sep 14 10:55:14 2020 -0500

    used map instead of native python loop

commit fbe9d64befd33120a8a5ba345ed584596b6d00ac
Author: Sanjay Yepuri <sanjay.yepuri@gmail.com>
Date:   Sat Sep 12 19:46:02 2020 -0500

    implemented lazy caching

commit ed76c1775e4c56134b15f37e44a035934fffee71
Author: Sanjay Yepuri <sanjay.yepuri@gmail.com>
Date:   Wed Sep 9 22:42:47 2020 -0500

    implemented range optimization; closes #3

commit 33dda630f06113d2527bc7d9e97b545ff79022f7
Author: Sanjay Yepuri <sanjay.yepuri@gmail.com>
Date:   Wed Sep 9 22:31:19 2020 -0500

    implemented odd number optimization; closes #2

commit 134d3743b0f7097fa9a89930901e651ef3c4ddbe
Author: Sanjay Yepuri <sanjay.yepuri@gmail.com>
Date:   Wed Sep 9 19:38:16 2020 -0500

    implemented basic collatz; closes #1

commit 38c662ace5d292aa7b3e24eaa86f42a37de5ea18
Author: Sanjay Yepuri <sanjay.yepuri@gmail.com>
Date:   Wed Sep 2 16:06:08 2020 -0500

    updated time estimate

commit 89fc8bb23956ad87e14bd65cf3a5e26ac02c5db4
Author: Glenn Downing <downing@cs.utexas.edu>
Date:   Tue Sep 1 18:06:46 2020 -0500

    another commit

commit 0a64f8d56f20d7d4096a37ccb79d6f07b176d459
Author: Glenn Downing <downing@cs.utexas.edu>
Date:   Mon Aug 31 20:12:56 2020 -0500

    first commit

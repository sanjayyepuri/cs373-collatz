#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, Tuple
from functools import lru_cache

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> Tuple[int, int]:
    """
    read two ints
    s a string
    return a tuple of two ints
    """
    a = s.split()
    return int(a[0]), int(a[1])


# ------------
# collatz_eval
# ------------


def collatz_eval(t: Tuple[int, int]) -> Tuple[int, int, int]:
    """
    t a tuple of two ints
    return a tuple of three ints
    """
    i, j = t

    # inputs must be strictly positive
    assert i > 0 and j > 0

    start, end = (i, j) if i <= j else (j, i)

    # Collatz range optimization
    m = end // 2 + 1
    start = max(start, m)

    # range must be ordered
    assert start <= end

    # using map and max is faster than native loops
    max_cycle = max(map(recur_cycle_len, range(start, end + 1)))

    return i, j, max_cycle


# ---------------
# recur_cycle_len
# ---------------

# use python functools lru_cache as a lazy cache
@lru_cache(100000000)
def recur_cycle_len(num: int) -> int:
    """
    num an int
    return the cycle length of num
    """
    if num == 1:
        # base case
        return 1

    if num % 2 == 0:
        # if the number is even
        return recur_cycle_len(num >> 1) + 1

    # odd number optimization to save a function call
    return recur_cycle_len(num + ((num + 1) >> 1)) + 2


# -------------
# collatz_print
# -------------


def collatz_print(sout: IO[str], t: Tuple[int, int, int]) -> None:
    """
    print three ints
    sout a writer
    t a tuple of three ints
    """
    i, j, v = t
    sout.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(sin: IO[str], sout: IO[str]) -> None:
    """
    sin  a reader
    sout a writer
    """
    for s in sin:
        collatz_print(sout, collatz_eval(collatz_read(s)))
